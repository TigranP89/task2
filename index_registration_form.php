<?php
include_once('users.php');

$userdata = new Users();
$utilitiesdata = new Utilities();

$fname = $utilitiesdata->testInput($_POST['fname']);
$lname = $utilitiesdata->testInput($_POST['lname']);
$email = $utilitiesdata->testInput($_POST['email']);
$password = $utilitiesdata->testInput($_POST['password']);
$repass = $utilitiesdata->testInput($_POST['repass']);

$fnamec = $lnamec = $emailc = '';
$errors = array();

if (empty($fname)){
    $errors['fname'] = 'User First name is empty<br>';        
} else {$fnamec = $fname;}
if (empty($lname)){
    $errors['lname'] = 'User Last name is empty<br>';       
} else {$lnamec = $lname;}
if (empty($email)){
    $errors['email'] = 'Email is empty<br>';        
} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors['email'] = 'Emai is invalid<br>';
    $emailc = $email;              
} else {$emailc = $email;}
if (empty($password)){
    $errors['password'] = 'Password is empty<br>';              
} 
if (empty($repass)){
    $errors['repass'] = 'Repeat password is empty<br>';      
}
if (!empty($password) && ($password != $repass)) {
    $errors['repass'] = 'Password and repeat password must match<br>'; 
}
if (!empty($password) && ($password == $repass)) {
    if(strlen($password) <= '8') {
        $errors['password'] = 'Password must contain at least 8 characters<br>';             
    } elseif (!preg_match("#[0-9]+#", $password)) {
        $errors['password'] = 'Password must contain at last 1 number<br>';              
    } elseif (!preg_match("#[A-Z]+#", $password)) {
        $errors['password'] = 'Password must contain at least 1 capital letter<br>';       
    } elseif (!preg_match("#[a-z]+#", $password)) {
        $errors['password'] = 'Password must contain at least 1 lowercase letter<br>';       
    }    
}

if(sizeof($errors) > 0) {
    include_once('viwes/registration/index');
} else {
    $userdata->registration($fname,$lname,$email,$password);
    session_start();  
    $_SESSION['email'] = $email;
    $_SESSION['password'] = $password;           

    include_once('login_autologin.php');
}

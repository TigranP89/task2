<!DOCTYPE html>
<html lang="en">
<head>    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Registration</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <script src="../../assets/errorValidation.js" defer></script>    
    <link rel="stylesheet" href="../../assets/styles.css">
</head>
<?php     
    $phpErrors = isset($errors) ? $errors : array();
    $fnamec = isset($fnamec) ? $fnamec : "";
    $lnamec = isset($lnamec) ? $lnamec : "";
    $emailc = isset($emailc) ? $emailc : "";
?>
<body>    
    <div class="container mt-5 mb-5">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-md-6">
                <div class="card px-5 py-5">
                    <main class="form-signin">                                 
                        <form id="registrationForm" class="needs-validation" name="registrationForm" action="/index_registration_form.php" method="POST">
                         
                            <div class="form-reg">                               
                                <div class="form-group">
                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" value="<?php echo isset($fnamec) ? $fnamec : "";?>">
                                    <div class='text-danger' id="fnameError" style="display: none;">User First name is empty</div>
                                    <div class='text-danger' id="fnamePhpError"><?php echo isset($phpErrors['fname']) ? $phpErrors['fname'] : ""; ?></div>
                                    <br><br>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" value="<?php echo isset($lnamec) ? $lnamec : "";?>">
                                    <div class='text-danger' id="lnameError" style="display: none;">User Last name is empty</div>
                                    <div class='text-danger' id="lnamePhpError"><?php echo isset($phpErrors['lname']) ?  $phpErrors['lname'] : ""; ?></div>
                                    <br><br>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo isset($emailc) ? $emailc : "";?>">
                                    <div class='text-danger' id="emailError" style="display: none;">Email is empty</div>
                                    <div class='text-danger' id="emailPhpError"><?php echo isset($phpErrors['email']) ? $phpErrors['email'] : ""; ?></div>
                                    

                                    <br><br>
                                </div>
                                <div class="form-group">            
                                    <input type="password" class="form-control" id="pass" name="password" placeholder="Password">
                                    <div class='text-danger' id="passError" style="display: none;">Password is empty</div>
                                    <div class='text-danger' id="passEmptyError" style="display: none;">Password is empty</div>
                                    <div class='text-danger' id="passPhpError"><?php echo isset($phpErrors['password']) ?  $phpErrors['password'] : ""; ?></div>
                                    <br><br>
                                </div>
                                <div class="form-group">           
                                    <input type="password" class="form-control" id="repass" name="repass" placeholder="Repeat password">
                                    <div class='text-danger' id="repassError" style="display: none;">Repeat password is empty</div>
                                    <div class='text-danger' id="repassPhpError"><?php echo isset($phpErrors['repass']) ?   $phpErrors['repass']  : ""; ?></div>
                                    <br><br>
                                </div>
                                <input id="submitInput" type="submit" value="Submit" class="btn btn-outline-success" name="submit">
                                <div class="form-group">           
                                    <div class="form-control my-5">
                                        Already registered ? Go to <a href="../login">Login</a>
                                    </div>
                                </div>
                            </div>                        
                        </form>                    
                    </main>
                </div>
            </div>
        </div>    
    </div>    
</body>
</html>
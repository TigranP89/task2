<!DOCTYPE html>
<html lang="en">
<head>    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Adding form</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <script src="assets/errorValidation.js" defer></script>    
    <link rel="stylesheet" href="assets/styles.css">
</head>
<body>
    <nav class="navbar navbar-expand navbar-dark bg-dark" aria-label="Second navbar example">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarsExample02">
                <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="user_list">Home</a>
                </li>                
                </ul>

                <div>
                    <form action="/logout_form.php" method="POST">
                        <button class="btn btn-danger float-end m-3">LOGOUT</button>	
                    </form>    
                </div>            
            </div>
        </div>
    </nav>
    
    <div class="jumbotron jumbotron-fluid">
        <div class="container">                
            <form action="addNewProduct" method="POST">
                <table class="table">
                    <tr aling="center">
                        <th colspan="2"><h1>Add New Product</h1></th>
                    </tr>
                    
                    <tr>
                        <th>Product name</th><td><input class="form-control" type="text" name="itemName" placeholder="Item name"> </td> 
                    </tr>
                    <tr>
                        <th>Description</th><td><textarea class="form-control" name="description" aria-label="With textarea" rows="10" style="height:100%;" placeholder="Description"></textarea></td> 
                    </tr>
                    <tr>
                        <th>Price,$</th><td><input class="form-control" type="text" name="price" placeholder="Price"> </td> 
                    </tr>
                </table>
                <div>	
                    <button class="btn btn-outline-success">Add Product</button>	
                </div>
            </form>  
        </div>                
    </div>
</body>
</html>    
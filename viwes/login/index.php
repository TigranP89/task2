<!DOCTYPE html>
<html lang="en">
<head>    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Login</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <script src="../../assets/errorValidation.js" defer></script>    
    <link rel="../../stylesheet" href="assets/styles.css">    
</head>
<?php
    $phpLoginErrors = isset($errors) ? $errors : array();
    $emailc = isset($emailc) ? $emailc : "";
?>
<body>
    <div class="container mt-5 mb-5">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-md-6">
                <div class="card px-5 py-5">
                    <main class="form-signin">                        
                        <form id="loginForm" class="needs-validation" name="loginForm" action="/login_form.php" method="POST">
                            <div class="form-reg">
                                <div class="form-group">  
                                    <input type="text"  class="form-control my-1" id="emailLog" name="email" placeholder="Login/email" value="<?php echo isset($emailc) ? $emailc : "";?>"/><br>
                                    <div class='text-danger' id="emailLogError" style="display: none;">Email can`t be empty</div>
                                    <div class='text-danger' id="emailLoginPhpError"><?php echo isset($phpLoginErrors['logEmail']) ? $phpLoginErrors['logEmail'] : ""; ?></div>
                                </div>
                            </div>

                            <div class="form-reg">
                                <div class="form-group">  
                                    <input type="password"  class="form-control my-1" id="passLog" name="password" placeholder="password" /><br>
                                    <div class='text-danger' id="passLogError" style="display: none;">Password can`t be empty</div>
                                    <div class='text-danger' id="passLoginPhpError"><?php echo isset($phpLoginErrors['logPass']) ? $phpLoginErrors['logPass'] : ""; ?></div>                                    
                                </div>
                            </div>

                            <input id="submitLogin" type="submit" value="login" class="btn btn-primary text-uppercase" name="login">
                            <div class="form-group">
                                <div class="form-control my-5">
                                    Back to <a href="../registration">Registration</a>
                                </div>
                            </div>  
                        </form>                        
                    </main>
                </div>
            </div>
        </div>    
    </div>    
</body>
</html>
<?php

include_once('users.php');

$userdata = new Users();
$utilitiesdata = new Utilities();

$email = $utilitiesdata->testInput($_POST['email']);
$password = $utilitiesdata->testInput($_POST['password']);
$account = $userdata->getAccount($email);

$emailc = '';
$errors = array();

if (empty($account)){  
    $errors['logEmail'] = 'No such user<br>';
}
if(empty($email)){
    $errors['logEmail'] = 'Email can`t be empty<br>';
} else {$emailc = $email;}
if(empty($password)){
    $errors['logPass'] = 'Password can`t be empty<br>';
} 
if(sizeof($errors) == 0) {
    if (!$userdata->login($email, $password)) {                    
        $errors['logEmail'] = 'Incorrect Email or Password<br>';
    }
}

if(sizeof($errors) > 0) {
    include_once('viwes/login');
} else {
    session_start();
   
    $_SESSION['id'] = $account['id'];
    $_SESSION['email'] = $account['email'];
    include_once('viwes/product/index.php');
}



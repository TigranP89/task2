$(document).ready(function(){
    $('#registrationForm').submit(function (evt) {        
        let valid = true;
        
        var fname = $("#fname").val();
        
        if(fname == ''){
            valid = false;
            $("#fnameError").show();            
        } else { $("#fnameError").hide();}


        var lname = $("#lname").val();

        if(lname == ''){
            valid = false;
            $("#lnameError").show();            
        } else { $("#lnameError").hide();}

        
        var email = $("#email").val();
        var pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

        if(email == ''){
            $("#emailError").html("Email is empty");
            valid = false;
            $("#emailError").show();
        } else if(email !='' && !pattern.test(email)){
            $("#emailError").html("Email is invalid");
            valid = false;
            $("#emailError").show();
        } else {
            $("#emailError").hide();
        }


        var password = $("#pass").val();
        var repass = $("#repass").val();

        if(password == ''){
            $("#passEmptyError").html("Password is empty");
            valid = false;
            $("#passEmptyError").show();
        } else { $("#passEmptyError").hide();}

        if(repass == ''){
            $("#repassError").html("Repeat password is empty");
            valid = false;
            $("#repassError").show();
        } else if(password != '' && password != repass){
            $("#repassError").html("Password and repeat password must match");
            valid = false;
            $("#repassError").show();
        } else { $("#repassError").hide();}

        if(password != '' && password == repass){
            if(password.length < 8){
                $("#passError").html("Password must contain at least 8 characters");
                valid = false;
                $("#passError").show();
            } else if(!password.match(/[A-Z]/g)){
                $("#passError").html("Password must contain at least 1 capital letter");
                valid = false;
                $("#passError").show();
            } else if(!password.match(/[a-z]/g)){
                $("#passError").html("Password must contain at least 1 lowercase letter");
                valid = false;
                $("#passError").show();
            } else if(!password.match(/\d/g)){
                $("#passError").html("Password must contain at last 1 number");
                valid = false;
                $("#passError").show();  
            }
        }  else { $("#passError").hide();}
      

        if(!valid) {
            evt.preventDefault();
        }
    });
    
    $('#loginForm').submit(function (evt) { 
        
        let valid = true;

        var logEmail = $("#emailLog").val();
        if(logEmail == ''){                     
            valid = false;
            $("#emailLogError").show();
        } else { $("#emailLogError").hide();}


        var logPass = $("#passLog").val();
        if(logPass == ''){               
            valid = false;
            $("#passLogError").show();
        } else { $("#passLogError").hide();}

        if(!valid) {
            evt.preventDefault();
        }
    });
 
});


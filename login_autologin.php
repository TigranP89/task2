<?php
session_start();
include_once('users.php');

$email = $_SESSION['email'];
$password = $_SESSION['password'];
$account = $userdata->getAccount($email);

if ($userdata->login($email, $password)){
    $_SESSION['id'] = $account['id'];
    $_SESSION['email'] = $account['email'];
    
    header('Location: viwes/product/index.php');
    exit();
}
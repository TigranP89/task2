<?php

class Connection {
    
    private static $instance;
    private function __construct() 
    {
    }
    private function __clone() 
    {
    }
    private function __wakeup()
    {        
    }
    
    public static function getInstance() {
        if(empty(self::$instance)) {
            define('DB_SERVER','localhost');
            define('DB_USER','root');
            define('DB_PASS','');
            define('DB_NAME','task2');

            try {
                self::$instance = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASS);
    
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);           
            } catch(PDOException $e) {
                echo "Connection failed: " . $e->getMessage();
            }
        }
        return self::$instance;
    }
}
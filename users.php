<?php
require_once ('connection.php');


class Users {
    public function registration($fname, $lname, $email, $password){ 
        $connection = Connection::getInstance();

        $hash = password_hash($password, PASSWORD_DEFAULT);
        
        $stmt = $connection->prepare("INSERT INTO users (fname, lname, email, password) VALUES (:fname, :lname, :email, :password)");
        $stmt->bindParam(':fname', $fname);
        $stmt->bindParam(':lname', $lname);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $hash);        
        $stmt->execute();             
    }
    
    public function login($email,$password) {           
        $account = $this->getAccount($email);
        return $account != NULL && password_verify($password, $account['password']);        
    }
    
    public function getAccount($email) {
        $connection = Connection::getInstance();
        try {            
            $stmt = $connection->prepare("SELECT * FROM users WHERE email = :email");
            $stmt->bindParam(':email', $email);
            $stmt->execute();
            $accounts = $stmt->fetchAll();
            if(sizeof($accounts) > 0){
                return $accounts[0];
            }
            return "";            
        } catch (PDOException $e) {
            return NULL;
        }
    }
}

class Utilities {    
    public function testInput($data) {        
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}